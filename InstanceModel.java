
public class InstanceModel {
	
	private String Nome;
	private String Sobrenome;
	private float Peso;
	private float Altura;
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public String getSobrenome() {
		return Sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		Sobrenome = sobrenome;
	}
	public float getPeso() {
		return Peso;
	}
	public void setPeso(float peso) {
		Peso = peso;
	}
	public float getAltura() {
		return Altura;
	}
	public void setAltura(float altura) {
		Altura = altura;
	}
	
}