import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Imc {
	
	private static Scanner input;
	private static ArrayList<InstanceModel> value = new ArrayList<InstanceModel>();

		
	public ArrayList<InstanceModel> loader(String file){
		
		try {
			input = new Scanner (Paths.get(file));
			
			input.nextLine();
			
			while(input.hasNext()) {
				String[] data = input.nextLine().split(";");
				
				InstanceModel dados = new InstanceModel();
				
				dados.setNome(data[0]);
				dados.setSobrenome(data[1]);
				dados.setPeso(Float.valueOf(data[2]));
				dados.setAltura(Float.valueOf(data[3]));
				
				
				
				value.add(dados);
			}
		}catch (Exception e) {
		System.out.println("Erro:"+ e.getMessage());
		}
		
		return value;
	}
}