import java.util.ArrayList;
import java.util.Locale;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols; 


public class InstanceView {
	
 

		public static void main(String[] args) {
			
		
			
			String padrao = "##.##";
			DecimalFormat df = new DecimalFormat(padrao);
			
			DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale("pt", "Brazil"));
			dfs.setDecimalSeparator('.');
			
			df = new DecimalFormat(padrao, dfs);
			
			ArrayList<InstanceModel> value = new Imc().loader("dataset.csv");
			
			
			System.out.println("Calculo IMC. Total: "+ value.size() + " linhas. \n");
			
			for(InstanceModel n : value)
				System.out.println(n.getNome().toUpperCase() + " " + n.getSobrenome().toUpperCase()
						+" "+(df.format(n.getPeso()/(n.getAltura()*n.getAltura()))));
				
			
			
		}
}
